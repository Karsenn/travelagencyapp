package com.example.travelagency.controller;

import com.example.travelagency.model.City;
import com.example.travelagency.model.CityDto;
import com.example.travelagency.model.Country;
import com.example.travelagency.model.HotelDto;
import com.example.travelagency.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path = "/admin/city/")
public class CityController {

    private final CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }


    @GetMapping(path = "/list")
    public String listCities(Model model) {
        model.addAttribute(
                "cityList", cityService.getAllCities());
        return "cityList";
    }

    @GetMapping(path = "/delete")
    public String deleteCity(@RequestParam(name = "cityId") Long id) {
        cityService.deleteCity(id);
        return "redirect:/admin/city/list";
    }

    @GetMapping(path = "/update")
    public String updateForm(@RequestParam(name = "cityId") Long id, Model model) {
        Optional<City> cityOptional = cityService.getCityById(id);
        if (cityOptional.isPresent()) {
            City city = cityOptional.get();
            model.addAttribute("city", city);
            return "cityUpdateForm";
        }
        return "redirect:/admin/city/list";
    }

    @PostMapping(path = "/update")
    public String updateCity(Model model, @Valid City city) {
        try {
            cityService.update(city);
        } catch (Exception e) {
            return "redirect:/admin/city/update?cityId=" + city.getId();
        }
        return "redirect:/admin/city/list";
    }


    @PostMapping(path = "/add")
    public String saveNewCity(Model model, @Valid CityDto city) {
        try {
            cityService.addCityToCountry(city);
        } catch (Exception e) {
            return "redirect:/admin/country/" + city.getCountryId() + "/add_city";
        }
        return "redirect:/admin/city/list";
    }


    @GetMapping(path = "/{city_id}/add_hotel")
    public String getAddCountryForm(@PathVariable("city_id") Long cityId,
                                    Model model) {
        model.addAttribute("hotel", new HotelDto());
        model.addAttribute("cityId", cityId);
        model.addAttribute("city", cityService.getCityById(cityId).orElseThrow(EntityNotFoundException::new));

        return "hotelForm";
    }
}
