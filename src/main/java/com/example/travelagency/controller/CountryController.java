package com.example.travelagency.controller;

import com.example.travelagency.model.CityDto;
import com.example.travelagency.model.Country;
import com.example.travelagency.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path = "/admin/country/")
public class CountryController {

    private CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping(path = "/list")
    public String listCities(Model model) {
        model.addAttribute(
                "countryList", countryService.getAllCountry());
        return "countryList";
    }

    @GetMapping(path = "/delete")
    public String deleteCountry(@RequestParam(name = "countryId") Long id) {
        countryService.deleteCountry(id);
        return "redirect:/admin/country/list";
    }

    @GetMapping(path = "/update")
    public String updateForm(@RequestParam(name = "countryId") Long id, Model model) {
        Optional<Country> countryOptional = countryService.getCountryById(id);
        if (countryOptional.isPresent()) {
            model.addAttribute("country", countryOptional.get());
            return "countryUpdateForm";
        }
        return "redirect:/admin/country/list";
    }

    @PostMapping(path = "/update")
    public String updateCountry(Model model, @Valid Country country){
        try{
            countryService.update(country);
        }catch (Exception e){
            model.addAttribute("country", country);
            return "countryForm";
        }
        return "redirect:/admin/country/list";
    }


    @PostMapping(path = "/add")
    public String saveNewCountry(Model model, @Valid Country country) {
        try{
            countryService.update(country);
        }catch (Exception e) {
            model.addAttribute("country", country);
            return "countryForm";
        }
        return "redirect:/admin/country/list";
    }

    @GetMapping(path = "/add")
    public String getAddCountryForm(Model model) {
        model.addAttribute("country", new Country());
        return "countryForm";
    }


    @GetMapping(path = "/{country_id}/add_city")
    public String getAddCountryForm(@PathVariable("country_id") Long countryId,
                                    Model model) {
        model.addAttribute("city", new CityDto());
        model.addAttribute("countryId", countryId);
        model.addAttribute("country", countryService.getCountryById(countryId).orElseThrow(EntityNotFoundException::new));

        return "cityForm";
    }


}