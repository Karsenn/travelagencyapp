package com.example.travelagency.controller;

import com.example.travelagency.model.Hotel;
import com.example.travelagency.model.HotelDto;
import com.example.travelagency.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path = "/admin/hotel/")
public class HotelController {

    private final HotelService hotelService;

    @Autowired
    public HotelController(HotelService hotelService) {
        this.hotelService = hotelService;
    }


    @GetMapping(path = "/list")
    public String listHotels(Model model) {
        model.addAttribute(
                "hotelList", hotelService.getAllHotels());
        return "hotelList";
    }

    @GetMapping(path = "/delete")
    public String deleteHotel(@RequestParam(name = "hotelId") Long id) {
        hotelService.deleteHotel(id);
        return "redirect:/admin/hotel/list";
    }

    @GetMapping(path = "/update")
    public String updateForm(@RequestParam(name = "hotelId") Long id, Model model) {
        Optional<Hotel> hotelOptional = hotelService.getHotelById(id);
        if (hotelOptional.isPresent()) {
            Hotel hotel = hotelOptional.get();
            model.addAttribute("hotel", hotel);
            return "hotelUpdateForm";
        }
        return "redirect:/admin/hotel/list";
    }

    @PostMapping(path = "/update")
    public String updateHotel(Model model, @Valid Hotel hotel) {
        try {
            hotelService.update(hotel);
        } catch (Exception e) {
            return "redirect:/admin/hotel/update?hotelId=" + hotel.getId();
        }
        return "redirect:/admin/hotel/list";
    }


    @PostMapping(path = "/add")
    public String saveNewHotel(Model model, @Valid HotelDto hotel) {
        try {
            hotelService.addHotelToCity(hotel);
        } catch (Exception e) {
            return "redirect:/admin/city/" + hotel.getCityId() + "/add_hotel";
        }
        return "redirect:/admin/hotel/list";
    }
}

