package com.example.travelagency.controller;

import com.example.travelagency.model.*;
import com.example.travelagency.service.CityService;
import com.example.travelagency.service.HotelService;
import com.example.travelagency.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/admin/trip/")
public class TripController {

    private final TripService tripService;
    private final HotelService hotelService;
    private final CityService cityService;

    @Autowired
    public TripController(TripService tripService, HotelService hotelService, CityService cityService) {
        this.tripService = tripService;
        this.hotelService = hotelService;
        this.cityService = cityService;
    }

    @GetMapping(path = "/list")
    public String listTrips(Model model) {
        model.addAttribute(
                "tripList", tripService.getAllTrips());
        return "tripList";
    }

    @GetMapping(path = "/delete")
    public String deleteTrip(@RequestParam(name = "tripId") Long id) {
        tripService.deleteTrip(id);
        return "redirect:/admin/trip/list";
    }

    @GetMapping(path = "/update")
    public String updateForm(@RequestParam(name = "tripId") Long id, Model model) {
        Optional<Trip> tripOptional = tripService.getTripById(id);
        if (tripOptional.isPresent()) {
            Trip trip = tripOptional.get();
            model.addAttribute("trip", trip);
            model.addAttribute("hotelsUp", hotelService.getAllHotels());
            model.addAttribute("citiesUp", cityService.getAllCities());
            return "tripUpdateForm";
        }
        return "redirect:/admin/trip/list";
    }

    @PostMapping(path = "/update")
    public String updateTrip(Model model, @Valid Trip trip) {
        try {
            tripService.update(trip);
        } catch (Exception e) {
            return "redirect:/admin/trip/update?cityId=" + trip.getId();
        }
        return "redirect:/admin/trip/list";
    }

    @GetMapping("/add")
    public String getTripForm(Model model) {
        model.addAttribute("tripObject", new TripDto());
        model.addAttribute("hotels", hotelService.getAllHotels());
        model.addAttribute("cities", cityService.getAllCities());
        return "tripForm";
    }

    @GetMapping("/addToCity")
    public String getTripForm(@RequestParam("city") Long cityId,
                              Model model) {
        TripDto tripDto = new TripDto();
        tripDto.setCityToId(cityId);
        model.addAttribute("tripObject", tripDto);
        model.addAttribute("hotels", hotelService.getAllHotels().stream().filter(hotel -> hotel.getCity().getId() == cityId).collect(Collectors.toList()));
        model.addAttribute("cities", cityService.getAllCities());
        model.addAttribute("city", cityId);
        return "tripForm";
    }


    @PostMapping(path = "/add")
    public String saveNewTrip(@Valid TripDto tripDto) {
        try {
            tripService.addTripToCityAndHotel(tripDto);
        } catch (Exception e) {
            return "redirect:/admin/trip/add";
        }
        return "redirect:/admin/trip/list";
    }
}