package com.example.travelagency.controller;

import com.example.travelagency.model.Trip;
import com.example.travelagency.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping(path = "/user/trip/")
public class UserTripController {

    private final TripService tripService;

    @Autowired
    public UserTripController(TripService tripService) {
        this.tripService = tripService;
    }

    @GetMapping(path = "/list")
    public String userListTrips(Model model) {
        model.addAttribute(
                "userTripList", tripService.getAllTrips());
        return "userTripList";
    }
    @GetMapping(path = "/details")
    public String userDetailsTrips(Model model, @RequestParam Long tripId) {
        model.addAttribute(
                "userTripDetails", tripService.getTripById(tripId).orElseThrow(EntityNotFoundException::new));
        return "userTripDetails";
    }
}
