package com.example.travelagency.controller;

import com.example.travelagency.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/user/")
public class AdminUserController {
    private AppUserService appUserService;

    @Autowired
    public AdminUserController(final AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @GetMapping("/list")
    public String getUserList(Model model){
        model.addAttribute("userList", appUserService.getAllUsers());

        return "userListView";
    }

    @GetMapping("/delete")
    public String deleteUser(@RequestParam(name = "userId") Long userId,
                             HttpServletRequest request) {

        String referer = request.getHeader("Referer");
        appUserService.deleteUser(userId);

        return "redirect:" + referer;
    }
}
