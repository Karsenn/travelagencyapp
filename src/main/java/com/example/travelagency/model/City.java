package com.example.travelagency.model;

import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    public City(String name, Country country) {
        this.name = name;
        this.country = country;
    }

    @ManyToOne
    private Country country;

    @OneToMany(mappedBy = "city", cascade = {CascadeType.REMOVE})
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Cascade({org.hibernate.annotations.CascadeType.DELETE})
    private Set <Hotel> hotels;

}
