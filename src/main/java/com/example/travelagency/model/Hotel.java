package com.example.travelagency.model;


import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.Id;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String address;
    private int standard;

    public Hotel(String name, String address, City city) {
        this.name = name;
        this.address = address;
        this.city = city;
    }

    @ManyToOne
    private City city;

    @OneToMany(mappedBy = "hotel", cascade = {CascadeType.REMOVE})
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Cascade({org.hibernate.annotations.CascadeType.DELETE})
    private Set<Trip> trips;
}

