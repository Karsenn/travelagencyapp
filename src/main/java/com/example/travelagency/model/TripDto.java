package com.example.travelagency.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TripDto {
    private String name;
    private Long cityfromId;
    private Long cityToId;
    private Long hotelId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate startTripDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat (pattern = "yyyy-MM-dd")
    private LocalDate endTripDate;

    private double price;
    private int passengers;
}
