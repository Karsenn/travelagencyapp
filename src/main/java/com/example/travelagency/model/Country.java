package com.example.travelagency.model;

import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long id;

    @Column(unique = true)
    private String name;

    @OneToMany(mappedBy = "country", cascade = {CascadeType.REMOVE})
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Cascade({org.hibernate.annotations.CascadeType.DELETE})
    private Set<City> cities;

    public Country(String name) {
        this.name = name;
    }
}
