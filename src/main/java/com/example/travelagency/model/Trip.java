package com.example.travelagency.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToOne
    private City cityFrom;


    @ManyToOne
    private City cityTo;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat (pattern = "yyyy-MM-dd")
    private LocalDate startTripDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat (pattern = "yyyy-MM-dd")
    private LocalDate endTripDate;

    private double price;
    private int passengers;

    @ManyToOne
    private Hotel hotel;

    public Trip(String name, City cityFrom, City cityTo, Hotel hotel, LocalDate startTripDate, LocalDate endTripDate, double price, int passengers) {
        this.name = name;
        this.cityFrom = cityFrom;
        this.cityTo = cityTo;
        this.hotel = hotel;
        this.startTripDate = startTripDate;
        this.endTripDate = endTripDate;
        this.price = price;
        this.passengers = passengers;
    }


}
