package com.example.travelagency.exception;

public class CountryNotFoundException extends RuntimeException {
    public CountryNotFoundException(final String message) {
        super(message);
    }
}
