package com.example.travelagency.exception;

public class PasswordsDoNotMatchException extends RuntimeException {
    public PasswordsDoNotMatchException(final String message) {
        super(message);
    }
}
