package com.example.travelagency.repository;

import com.example.travelagency.model.AppUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppUserRoleRepository extends JpaRepository<AppUserRole, Long> {
    boolean existsByName(String name);

    AppUserRole findByName(String role);
}
