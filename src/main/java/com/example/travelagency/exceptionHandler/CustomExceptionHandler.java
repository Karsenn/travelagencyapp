package com.example.travelagency.exceptionHandler;


import com.example.travelagency.exception.PasswordNotLongEnoughException;
import com.example.travelagency.exception.PasswordsDoNotMatchException;
import com.example.travelagency.model.ErrorDetails;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;

@ControllerAdvice
public class CustomExceptionHandler {
    @ExceptionHandler(PasswordsDoNotMatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDetails handlePasswordsDoNotMatchException(final PasswordsDoNotMatchException exp) {
        return new ErrorDetails(LocalDateTime.now(), exp.getMessage(), null);
    }

    @ExceptionHandler(PasswordNotLongEnoughException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDetails handlePasswordNotLongEnoughException(final PasswordNotLongEnoughException exp) {
        return new ErrorDetails(LocalDateTime.now(), exp.getMessage(), null);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDetails handleUsernameNotFoundException(final UsernameNotFoundException exp) {
        return new ErrorDetails(LocalDateTime.now(), exp.getMessage(), null);
    }

}
