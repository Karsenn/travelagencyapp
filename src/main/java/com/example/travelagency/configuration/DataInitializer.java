package com.example.travelagency.configuration;

import com.example.travelagency.model.AppUser;
import com.example.travelagency.model.AppUserRole;
import com.example.travelagency.repository.AppUserRepository;
import com.example.travelagency.repository.AppUserRoleRepository;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {

    private AppUserRoleRepository userRoleRepository;
    private AppUserRepository appUserRepository;
    private PasswordEncoder passwordEncoder;


    @Autowired
    public DataInitializer(final AppUserRoleRepository userRoleRepository, final AppUserRepository appUserRepository, final PasswordEncoder passwordEncoder) {
        this.userRoleRepository = userRoleRepository;
        this.appUserRepository = appUserRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void onApplicationEvent(final ContextRefreshedEvent contextRefreshedEvent) {
        createRoleIfNotExists("ADMIN");
        createRoleIfNotExists("USER");

        createUserIfNotExists("admin", "admin", "ADMIN");
        createUserIfNotExists("user", "user", "USER");

    }

    private void createUserIfNotExists(final String username, final String password, final String... roles) {
        if (!appUserRepository.existsByEmail(username)) {
            AppUser appUser = new AppUser();
            appUser.setEmail(username);
            appUser.setPassword(passwordEncoder.encode(password));

            appUser.setRoles(new HashSet<>(findRoles(roles)));

            appUserRepository.save(appUser);
        }
    }

    private List<AppUserRole> findRoles(final String[] roles) {
        List<AppUserRole> userRoles = new ArrayList<>();
        for (String role : roles) {
            userRoles.add(userRoleRepository.findByName(role));
        }
        return userRoles;
    }

    private void createRoleIfNotExists(final String roleName) {
        if (!userRoleRepository.existsByName(roleName)) {
            AppUserRole role = new AppUserRole();
            role.setName(roleName);

            userRoleRepository.save(role);
        }

    }

}

