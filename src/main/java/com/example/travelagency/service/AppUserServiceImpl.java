package com.example.travelagency.service;

import com.example.travelagency.exception.PasswordNotLongEnoughException;
import com.example.travelagency.exception.PasswordsDoNotMatchException;
import com.example.travelagency.model.AppUser;
import com.example.travelagency.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppUserServiceImpl implements AppUserService {

    private AppUserRepository appUserRepository;
    private PasswordEncoder passwordEncoder;
    private AppUserRoleServiceImpl appUserRoleServiceImpl;

    @Autowired
    public AppUserServiceImpl(final AppUserRepository appUserRepository, final PasswordEncoder passwordEncoder, final AppUserRoleServiceImpl appUserRoleServiceImpl) {
        this.appUserRepository = appUserRepository;
        this.passwordEncoder = passwordEncoder;
        this.appUserRoleServiceImpl = appUserRoleServiceImpl;
    }

    @Override
    public void registerUser(final String username, final String password, final String passwordConfirm) {
        if (!password.equals(passwordConfirm)) {
            throw new PasswordsDoNotMatchException("'Password' and 'Confirm Password' do not match.");
        }

        int minimumPasswordLength = 8;

        if (password.length() < minimumPasswordLength) {
            throw new PasswordNotLongEnoughException("Password must be at least " + minimumPasswordLength + " characters long");
        }

        // create a new user and assign values
        AppUser appUser = new AppUser();
        appUser.setEmail(username);
        appUser.setPassword(passwordEncoder.encode(password));

        // set permissions
        appUser.setRoles(appUserRoleServiceImpl.getDefaultUserRoles());

        // save to database
        appUserRepository.save(appUser);
    }

    @Override
    public List<AppUser> getAllUsers() {
        return appUserRepository.findAll();
    }

    @Override
    public void deleteUser(final Long userId) {
        appUserRepository.deleteById(userId);
    }
}
