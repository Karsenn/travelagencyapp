package com.example.travelagency.service;

import com.example.travelagency.model.City;
import com.example.travelagency.model.Hotel;
import com.example.travelagency.model.Trip;
import com.example.travelagency.model.TripDto;
import com.example.travelagency.repository.CityRepository;
import com.example.travelagency.repository.HotelRepository;
import com.example.travelagency.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TripServiceImpl implements TripService {

    private final TripRepository tripRepository;
    private final HotelRepository hotelRepository;
    private final CityRepository cityRepository;

    @Autowired
    public TripServiceImpl(TripRepository tripRepository, HotelRepository hotelRepository, CityRepository cityRepository) {
        this.tripRepository = tripRepository;
        this.hotelRepository = hotelRepository;
        this.cityRepository = cityRepository;
    }

    @Override
    public List<Trip> getAllTrips() {
        return tripRepository.findAll();
    }

    @Override
    public void deleteTrip(Long tripId) {

        tripRepository.deleteById(tripId);
    }

    @Override
    public Optional<Trip> getTripById(Long id) {
        return tripRepository.findById(id);
    }

    @Override
    public void update(Trip trip) {
        Optional<Trip> tripOptional = tripRepository.findById(trip.getId());
        if (tripOptional.isPresent()) {
            Trip tripEdited = tripOptional.get();
            tripEdited.setName(trip.getName());
            tripEdited.setCityFrom(trip.getCityFrom());
            tripEdited.setCityTo(trip.getCityTo());
            tripEdited.setHotel(trip.getHotel());
            tripEdited.setStartTripDate(trip.getStartTripDate());
            tripEdited.setEndTripDate(trip.getEndTripDate());
            tripEdited.setPrice(trip.getPrice());
            tripEdited.setPassengers(trip.getPassengers());
            tripRepository.save(tripEdited);
        }

    }

    @Override
    public void addTripToCityAndHotel(TripDto tripDto) {
        Hotel hotel = hotelRepository.getOne(tripDto.getHotelId());
        City cityFrom = cityRepository.getOne(tripDto.getCityfromId());
        City cityTo = cityRepository.getOne(tripDto.getCityToId());

        Trip trip = new Trip(tripDto.getName(), cityFrom, cityTo, hotel,
                tripDto.getStartTripDate(), tripDto.getEndTripDate(), tripDto.getPrice(), tripDto.getPassengers());

        trip = tripRepository.save(trip);

        hotel.getTrips().add(trip);
        hotelRepository.save(hotel);
    }
}
