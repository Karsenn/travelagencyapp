package com.example.travelagency.service;



import com.example.travelagency.model.Hotel;
import com.example.travelagency.model.HotelDto;

import java.util.List;
import java.util.Optional;

public interface HotelService {

    List<Hotel> getAllHotels();
    void deleteHotel(Long hotelId);
    Optional<Hotel> getHotelById(Long id);
    void update(Hotel hotel);
    void addHotelToCity(HotelDto hotel);
}
