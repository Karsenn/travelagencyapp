package com.example.travelagency.service;

import com.example.travelagency.model.Trip;
import com.example.travelagency.model.TripDto;

import java.util.List;
import java.util.Optional;

public interface TripService {

    List<Trip> getAllTrips();
    void deleteTrip(Long tripId);
    Optional<Trip> getTripById(Long id);
    void update(Trip trip);
    void addTripToCityAndHotel(TripDto trip);
}
