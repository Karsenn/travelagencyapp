package com.example.travelagency.service;

import com.example.travelagency.model.City;
import com.example.travelagency.model.Hotel;
import com.example.travelagency.model.HotelDto;
import com.example.travelagency.repository.CityRepository;
import com.example.travelagency.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HotelSerivceImpl implements HotelService {

    private final CityRepository cityRepository;
    private final HotelRepository hotelRepository;

    @Autowired
    public HotelSerivceImpl(CityRepository cityRepository, HotelRepository hotelRepository) {
        this.cityRepository = cityRepository;
        this.hotelRepository = hotelRepository;
    }

    @Override
    public List<Hotel> getAllHotels() {
        return hotelRepository.findAll();
    }

    @Override
    public void deleteHotel(Long hotelId) {
        hotelRepository.deleteById(hotelId);

    }

    @Override
    public Optional<Hotel> getHotelById(Long id) {
        return hotelRepository.findById(id);
    }

    @Override
    public void update(Hotel hotel) {
        Optional<Hotel> hotelOptional = hotelRepository.findById(hotel.getId());
        if (hotelOptional.isPresent()) {
            Hotel hotelEdited = hotelOptional.get();
            hotelEdited.setName(hotel.getName());
            hotelEdited.setAddress(hotel.getAddress());
            hotelEdited.setStandard(hotel.getStandard());
            hotelRepository.save(hotelEdited);
        }
    }

    @Override
    public void addHotelToCity(HotelDto hotelDto) {
        City city = cityRepository.getOne(hotelDto.getCityId());

        Hotel hotel = new Hotel(hotelDto.getName(), hotelDto.getAddress(), city);
        hotel.setStandard(hotelDto.getStandard());

        hotel = hotelRepository.save(hotel);
        city.getHotels().add(hotel);
        cityRepository.save(city);
    }
}
