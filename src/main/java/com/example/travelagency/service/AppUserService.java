package com.example.travelagency.service;

import com.example.travelagency.model.AppUser;

import java.util.List;

public interface AppUserService {

    void registerUser(String username, String password, String passwordConfirm);

    List<AppUser> getAllUsers();

    void deleteUser(Long userId);
}
