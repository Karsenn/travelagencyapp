package com.example.travelagency.service;

import com.example.travelagency.model.Country;

import java.util.List;
import java.util.Optional;

public interface CountryService {

    List<Country> getAllCountry();
    void deleteCountry(Long countryId);
    void save(Country country);
    Optional<Country> getCountryById(Long id);
    void update(Country country);
}
