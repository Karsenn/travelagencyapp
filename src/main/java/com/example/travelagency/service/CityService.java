package com.example.travelagency.service;

import com.example.travelagency.model.City;
import com.example.travelagency.model.CityDto;

import java.util.List;
import java.util.Optional;

public interface CityService {

    List<City> getAllCities();
    void deleteCity(Long countryId);
    Optional<City> getCityById(Long id);
    void update(City city);
    void addCityToCountry(CityDto city);
}
