package com.example.travelagency.service;

import com.example.travelagency.model.AppUserRole;

import java.util.Set;

public interface AppUserRoleService {
    Set<AppUserRole> getDefaultUserRoles();
}
