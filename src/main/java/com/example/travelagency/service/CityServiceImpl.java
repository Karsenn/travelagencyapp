package com.example.travelagency.service;

import com.example.travelagency.exception.CountryNotFoundException;
import com.example.travelagency.model.City;
import com.example.travelagency.model.CityDto;
import com.example.travelagency.model.Country;
import com.example.travelagency.repository.CityRepository;
import com.example.travelagency.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CityServiceImpl implements CityService {


    private final CityRepository cityRepository;
    private final CountryRepository countryRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository, CountryRepository countryRepository) {
        this.cityRepository = cityRepository;
        this.countryRepository = countryRepository;
    }


    public List<City> getAllCities() {
        return cityRepository.findAll();
    }

    @Override
    public void deleteCity(Long cityId) {
        cityRepository.deleteById(cityId);
    }

    @Override
    public Optional<City> getCityById(Long id) {
        return cityRepository.findById(id);
    }

    @Override
    public void update(City city) {
        Optional<City> cityOptional = cityRepository.findById(city.getId());
        if (cityOptional.isPresent()) {
            City cityEdited = cityOptional.get();
            cityEdited.setName(city.getName());
            cityRepository.save(cityEdited);
        }
    }

    @Override
    public void addCityToCountry(CityDto cityDto) {
        Country country = countryRepository.getOne(cityDto.getCountryId());

        City city = new City(cityDto.getName(), country);
        city = cityRepository.save(city);
        country.getCities().add(city);
        countryRepository.save(country);
    }
}
