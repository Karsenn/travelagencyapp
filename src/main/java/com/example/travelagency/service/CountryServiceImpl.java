package com.example.travelagency.service;

import com.example.travelagency.model.City;
import com.example.travelagency.model.Country;
import com.example.travelagency.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getAllCountry() {
        return countryRepository.findAll();
    }

    @Override
    public void save(Country country) {

        if (!countryRepository.existsByName(country.getName())) {
//          todo: znim zapiszesz country sprawdz czy takie istnieje (napisz metode exists na repository, ktora sprawdza
//          todo: czy kraj o danej nazwie istnieje
            countryRepository.save(country);
        }
    }

    @Override
    public void deleteCountry(Long countryId) {
        countryRepository.deleteById(countryId);
    }

    @Override
    public Optional<Country> getCountryById(Long id) {
        return countryRepository.findById(id);
    }

    @Override
    public void update(Country country) {
        countryRepository.save(country);
    }
}
